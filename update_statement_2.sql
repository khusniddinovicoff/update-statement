
with changed_customer as (
    update customer set first_name = 'AA', last_name = 'KK', address_id = 5
        where customer_id = 5
        returning *)
update rental
set inventory_id = 7
where customer_id in (select customer_id from changed_customer);

with changed_customer as (
    update customer set first_name = 'AA', last_name = 'KK', address_id = 5
        where customer_id = 5
        returning *)
update payment
set amount = 11
where customer_id in (select customer_id from changed_customer);